# qcompjs

## Whats in the box?
Well... not a cat. At least not yet.\
QComp.js is a JavaScript library build to simulate quantum logic.\
It employs Paul Dirac's bra-ket notation and matrix and vector mathematics to simulate the quantum processes.

---
## Usage
To initialise a new quantum system (quantum computer), use the QC() constructor with a bra-ket string for arguments:\
`const qc = new QC("|01101>");`